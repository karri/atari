#include <lynx.h>
#include <tgi.h>
#include <6502.h>
#include <joystick.h>
#include <stdlib.h>

extern void tgi_atarixy(int x, int y, char *msg);

typedef struct {
    SCB_REHV sprite;
    PENPAL_4;
} isprite_t;

void main()
{
    unsigned char joy;
    int j = 0;

    joy_install(&lynx_stdjoy);
    tgi_install(&lynx_160_102_16);
    tgi_init();
    CLI();
    while (1) {
        while (tgi_busy())
            ;
        joy = joy_read(JOY_1);
        if (JOY_BTN_UP(joy)) {
            j++;
        }
        if (JOY_BTN_DOWN(joy)) {
            j--;
        }
        tgi_clear();
        tgi_setcolor(COLOR_WHITE);
        tgi_bar(0, 0, 159, 101);
        tgi_setcolor(COLOR_DARKBROWN);
        tgi_atarixy(1, j+10, "The Atari font");
        tgi_atarixy(1, j+30, " !\"#$%&'()*+,-./");
        tgi_atarixy(1, j+40, "0123456789:;<=>?");
        tgi_atarixy(1, j+50, "@ABCDEFGHIJKLMNO");
        tgi_atarixy(1, j+60, "PQRSTUVWXYZ_");
        tgi_atarixy(1, j+70, "`abcdefghijklmno");
        tgi_atarixy(1, j+80, "pqrstuvwxyz{|}~.");
        tgi_atarixy(1, j+95, "The quick brown fox jumps over a lazy old dog.");
        tgi_updatedisplay();
    
    }
}

